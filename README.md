# Data Clustering

A demo showcasing the PAM Regression algorithm written in R.

## goals

- test a non-standard clustering algorithm.
- Apply data manipulation knowledge.
- Get more comfortable writing code in R.

## Created by: 

- Isabelle Sonke 
